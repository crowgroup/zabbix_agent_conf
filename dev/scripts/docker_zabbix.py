#!/usr/bin/python
import time

import docker
import json
from functools import reduce
import argparse
import shelve
import filelock
import sys
import os
from filelock import Timeout, FileLock
from docker.errors import NotFound
from numpy import float32

docker_client = docker.from_env()

cache_file = os.path.abspath(__file__) + '.cache'
lock_file = os.path.abspath(__file__) + '.lock'


def get_dict_val(d, attr):
    val = reduce(lambda a, b: a.get(b, {}), attr.split("."), d)
    if val == {}:
        return "*NOT FOUND"
    return val


def _get_container(cid):
    return docker_client.containers.get(cid)


def _get_container_stats(cid):
    lock = FileLock(lock_file)
    with lock:
        db = shelve.open(cache_file)
        stats = db.get(cid, None)
        if stats is None or time.time() - stats.get('cache_time', 0) > 60:
            cont = _get_container(cid)
            stats = cont.stats(stream=False)
            stats['cache_time'] = time.time()
            db[cid] = stats
        db.close()
    return stats


def _get_containers_list():
    return docker_client.containers.list(all=False, ignore_removed=True)


def list_containers(*args):
    containers = _get_containers_list()
    a = {"data": [{"{#DCNAME}": c.name, "{#DCID}": c.short_id, "{#DIMAGE}": c.image.tags[0]} for c in containers]}
    print(json.dumps(a, indent=2))


def get_container_val(cmd):
    try:
        print(getattr(_get_container(cmd.container_id), cmd.field))
    except NotFound:
        print("Container not found")


def get_container_attr(cmd):
    try:
        print(get_dict_val(docker_client.containers.get(cmd.container_id).attrs, cmd.field))
    except NotFound:
        print("Container not found")


def get_container_stats(cmd):
    try:
        stats = _get_container_stats(cmd.container_id)
        print(get_dict_val(stats, cmd.field))
    except NotFound:
        print("Container not found")


def get_container_cpu(cmd):
    try:
        stats = _get_container_stats(cmd.container_id)
        cpu_percent = 0.0
        cpu_delta = float32(get_dict_val(stats, 'cpu_stats.cpu_usage.total_usage')) - \
                    float32(get_dict_val(stats, 'precpu_stats.cpu_usage.total_usage'))
        system_delta = float32(get_dict_val(stats, 'cpu_stats.system_cpu_usage')) - \
                       float32(get_dict_val(stats, 'precpu_stats.system_cpu_usage'))

        if system_delta > 0.0 and cpu_delta > 0.0:
            cpu_percent = (cpu_delta / system_delta) * \
                          len(get_dict_val(stats, 'cpu_stats.cpu_usage.percpu_usage')) * 100.
        print("%.2f" % cpu_percent)
    except NotFound:
        print("Container not found")
    except ValueError:
        print("0.00")


parser = argparse.ArgumentParser(
    description="Zabbix Docker Query Tool"
)
subparsers = parser.add_subparsers(help='What to query')

parser_containers = subparsers.add_parser('containers', help="List containers")
parser_containers.set_defaults(func=list_containers)
parser_container = subparsers.add_parser('container', help="container properties")

parser_container.add_argument("container_id", help="Id of the container")
parser_container.add_argument("field", help="field to query in dot notation")
parser_container.set_defaults(func=get_container_val)

parser_attrs = subparsers.add_parser('attrs', help="container attributes")
parser_attrs.add_argument("container_id", help="Id of the container")
parser_attrs.add_argument("field", help="field to query in dot notation")
parser_attrs.set_defaults(func=get_container_attr)

parser_stats = subparsers.add_parser('stats', help="container stats")
parser_stats.add_argument("container_id", help="Id of the container")
parser_stats.add_argument("field", help="field to query in dot notation")
parser_stats.set_defaults(func=get_container_stats)

parser_cpu = subparsers.add_parser('cpu', help="container cpu usage")
parser_cpu.add_argument("container_id", help="Id of the container")
parser_cpu.set_defaults(func=get_container_cpu)

args = parser.parse_args()
args.func(args)
